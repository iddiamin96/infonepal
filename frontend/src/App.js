import React from "react";
import { Route, Routes} from "react-router-dom";
import './App.css';
import 'antd/dist/antd.css';
//import component from component location
import AppHome from "./views/Home.js";
import Login from "./pages/auth/login";
import SignUp from "./pages/auth/signup";
import AboutUs from "./views/AboutUs";
import Categorys from "./views/Category";
import CompanyListings from "./views/CompanyListing";
import ListYourCompany from "./views/ListYourCompany";
import CompanyDetails from "./views/companyDetail/[id]";
import Search from "./views/Search";
import store from "./store";
import { loadUser} from "./actions/userAction";
import UserOptions from "./pages/common/UserOptions";
import { useSelector } from "react-redux";
import Profile from "./pages/auth/Profile";
import UpdateProfile from "./pages/auth/UpdateProfile";
import UpdatePassword from "./pages/auth/UpdatePassword";
import ForgotPassword from "./pages/auth/ForgotPassword";
import ProtectedRoute from "./Route/ProtectedRoute";
import ResetPassword from "./pages/auth/ResetPassword";
import Review from "./views/companyDetail/Review";
import Dashboard from "./pages/admin/Dashboard";
import CompanyLists from "./pages/admin/CompanyLists";
import NewCompany from "./pages/admin/NewCompany";
import UpdateCompany from "./pages/admin/UpdateCompany";
import UsersList from "./pages/admin/user/UsersList";
import UpdateUser from "./pages/admin/user/UpdateUser";


const App = () => {
  store.dispatch(loadUser());
  return (
      <Routes>
        <Route path="/" element={<AppHome />}/>
        <Route path="/login" element={<Login />}/>
        <Route path="/listyourcompany" element={<ListYourCompany />}/>
        <Route path="/category" element={<Categorys />}/>
        <Route path="/company" element={<CompanyListings />}/>
        <Route path="/aboutus" element={<AboutUs />}/>
        <Route path="/signup" element={<SignUp />}/>
        <Route path="/companylist/:id" element={<CompanyDetails />}/>
        <Route path="/search" element={<Search />}/>
        <Route path="/account" element={<Profile />} />
        <Route exact path="/profile/update" element={<UpdateProfile />} />
        <Route exact path="/password/update" element={<UpdatePassword />} />
        <Route exact path="/password/forgot" element={<ForgotPassword />} />
        <Route exact path="/password/reset/:token" element={<ResetPassword />} />
        <Route exact path="/review" element={<Review />} />
        <Route exact path="/admin/dashboard" element={<Dashboard />} />
        <Route exact path="/admin/companylists" element={<CompanyLists />} />
        <Route exact path="/admin/companylist" element={<NewCompany />} />
        <Route exact path="/admin/companylist/:id" element={<UpdateCompany />} />
        <Route exact path="/admin/users" element={<UsersList />} />
        <Route exact path="/admin/users/:id" element={<UpdateUser />} />



      </Routes>
  );
}

export default App;
