import React from "react";
import { Menu, Image, Button} from 'antd';
import { Link } from "react-router-dom";
import "./header.css"

function AppHeader(){
    return (
        <div className=  "container-fluid">
            <section className = "header">
                    <div className="logo"> 
                        <a href="/"><img src="https://i.ibb.co/6thpb44/NEPAL-removebg-preview.png" width={100}/> </a>
                    </div>
                    <Menu className="menu" mode="horizontal" defaultSelectedKeys={['Home']}>
                        <Menu.Item key="home"><Link to="/">Home</Link></Menu.Item>
                        <Menu.Item key="Category"><Link to="/Category">Category</Link></Menu.Item>
                        <Menu.Item key="CompanyListing"><Link to="/Company">Company Listing</Link></Menu.Item>
                        <Menu.Item key="About Us"> <Link to="/aboutus">About Us</Link></Menu.Item>
                    </Menu>
                    <div className="header-button">
                        <Button type="primary"><Link to="/listyourcompany">List Company</Link></Button>
                        <Button type="primary"><Link to="/login">Login</Link></Button>
                    </div>
            </section>
        </div>
    );
}

export default AppHeader;