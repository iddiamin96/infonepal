import React, {Fragment, useState } from 'react';
import { Row, Col, Tooltip, Avatar } from "antd";
import { connect,useDispatch, useSelector } from "react-redux";
import { LogoutOutlined, UserOutlined, DashboardOutlined } from '@ant-design/icons';
import { useNavigate } from "react-router-dom";
import { SpeedDial, SpeedDialAction } from "@material-ui/lab";
import { logout } from "../../actions/userAction";
import "./header.css";

const UserOptions = ({user}) => {
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const options = [
        { icon: <UserOutlined />, name: "Profile", func: account },
        { icon: <LogoutOutlined />, name: "Logout", func: logoutUser },
      ];

    if (user.role === "admin") {
        options.unshift({
        icon: <DashboardOutlined />,
        name: "Dashboard",
        func: dashboard,
        });
    }
    
    function dashboard() {
        window.location.href = "/admin/dashboard";
        // navigate.push("/admin/dashboard");
      }
    
      function account() {
        window.location.href = "/account";
        // navigate.push("/account");
      }

      function logoutUser() {
        dispatch(logout());
      }

    return (
        <Fragment>
            <SpeedDial
                ariaLabel="SpeedDial tooltip example"
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
                open={open}
                direction="down"
                className="speedDial"
                icon={
                  <img
                    className="speedDialIcon"
                    src={user.avatar[0].url }
                    alt="Profile"
                    
                  />
                
                }
            >
            {options.map((item) => (
                <SpeedDialAction
                    key={item.name}
                    icon={item.icon}
                    tooltipTitle={item.name}
                    onClick={item.func}
                    tooltipOpen={window.innerWidth <= 200 ? true : false}
                />
            ))}
            </SpeedDial>
    </Fragment>

    //     <Tooltip
    //     title={
    //         <>
    //             <div>
    //                 <div>
    //                     <a href={localStorage.user_usertype === "2" ? ("/account") : ("/myprofile")} style={{ color: "white" }}>Profile</a>
    //                 </div>
    //                 <hr />
    //                 <div>
    //                     <a style={{ color: "white" }} href="/changepassword">Change Password</a>
    //                     <hr />
    //                 </div>
    //             </div>
    //         </>
    //     }
    //     placement="bottom"
    // >
    //     <Avatar
    //         src={user.avatar.url ? user.avatar.url : "/logo512.png"}
    //         alt="profile"
    //         style={{
    //             marginTop: 17,
    //             marginRight: 50,
    //             cursor: "pointer",
    //         }}
    //         size={40}
    //     />
    // </Tooltip>
    )
}

export default UserOptions
