import React from "react";
import "./footer.css";

const AppFooter = () => {
    return (
        <footer id="footer">
            <div className="left"> 
                <a href="home.js"><img src="https://i.ibb.co/6thpb44/NEPAL-removebg-preview.png" width={100}/> </a>
                <h4>Info Nepal Provide Information of the company with their rating</h4>
            </div >
            <div className="middle">
                <h2>Important Links</h2>
                <h4>About</h4>
                <h4>Faqs</h4>
                <h4>Term and Comdition</h4>
            </div>
            <div className="right">
                <h2>Follow Us</h2>
                <a href="http://facebook.com">Facebook</a>
                <a href="http://instagram.com">Instagram</a>
            </div>
        </footer>
    )
}

export default AppFooter