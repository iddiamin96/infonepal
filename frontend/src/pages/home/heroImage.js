import React from "react";
import { Carousel,Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

const items = [
    {
        key:'1',
        title: 'list company',
        content: 'list your company according to locations',
    },
    {
        key:'1',
        title: 'list company',
        content: 'list your company according to locations',
    },
    {
        key:'1',
        title: 'list company',
        content: 'list your company according to locations',
    },
    {
        key:'1',
        title: 'list company',
        content: 'list your company according to locations',
    },
]

function AppHero() {
    return (
        <div className="heroBlock">
            <Carousel>
                {items.map(item => {
                    return (
                        <div className="container-fluid" key={item.key}>
                            <div className="content">
                                <h3>{item.title}</h3>
                                <p>{item.content}</p>
                                <div className="btnHolder">
                                    <Button type="primary"> Primary Button </Button>
                                    <Button type="primary"> Learn Button </Button>
                                </div>
                             </div>
                        </div>
                    );})}
            </Carousel>
        </div>
    );
}

export default AppHero;