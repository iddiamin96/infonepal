import React from "react";
import {Row, Col, Rate} from 'antd';
import './companyListing.css';
import { Link } from "react-router-dom";

const options = {
    edit:false,
    color:"red",
    activeColor: "tomato",
    size:window.innerWidth < 600 ? 20 :25,
    value:2.5,
    isHalf: true,
};

const CompanyListing = ({companylist}) => {

    console.log('companylist')
    console.log(companylist)
    return (
        <div className="block CompanyBlock">
            <div className="container-fluid1">
                {companylist.map((res) => {
                    return (
                    <Link className="listing" to={`/companylist/${res._id}`}>
                        <img src={res.logo[0].url} alt={res.name} />
                        <p> {res.name}</p>
                        <h4>{res.category} </h4>
                        <Rate allowHalf defaultValue={res.ratings} />
                    </Link>
                )})}
            </div>
        </div>
    );
}

export default CompanyListing;