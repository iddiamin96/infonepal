import React, { useState } from 'react';
import { Input, Button, Select,} from 'antd';
import './HeroSearchSection.css';
import { CopyOutlined } from '@ant-design/icons';

const { Option } = Select;
const options = [
    {
      value: 'agriculture',
      label: 'agriculture',
      children: [
        {
          value: 'xyz fruit shop',
          label: 'fruit shop',
          children: [
            {
              value: 'apple',
              label: 'apple',
            },
          ],
        },
      ],
    },
    {
      value: 'Hospital',
      label: 'Hospital',
      children: [
        {
          value: 'KMC',
          label: 'KMC',
          children: [
            {
              value: 'Ortho department',
              label: 'Ortho department',
            },
          ],
        },
      ],
    },
  ];

function HeroSearchSection() {
    return (
        <div className="hero-image">
            <div className="hero-text">
                <h1>Search Businesses</h1>
                <Input.Group compact>
                <Select defaultValue="agriculture">
                    <Option value="agriculture">agriculture</Option>
                    <Option value="Hospital">Hospital</Option>
                </Select>
                <Input style={{ width: '50%' }} defaultValue="Hospital, kmc" />
                </Input.Group>
                <Button type="primary">SEARCH</Button>
            </div>
        </div>  
    )
}

export default HeroSearchSection
