import React from "react";
import {Row, Col} from 'antd';
import { Card } from 'antd';
import "./businessCategory.css";

const { Meta } = Card;


function BusinessCategory() {
    return (
        <div className="block FeatureBlock bgGray">
            <div className="container-fluid">
                <div className="titleHolder">
                    <h2> Business Category </h2>
                    <p> List of Business According to Category </p>
                </div>
                <Row className="category" gutter={[32, 32]}>
                    <Col span={8}>
                        <Card
                            hoverable
                            cover={<img alt="example" src="https://i.ibb.co/MN8whG2/image-63.png"/>}>
                            <Meta title="Shopping"/>
                        </Card>,
                    </Col>
                    <Col span={8}>
                        <Card
                            hoverable
                            cover={<img alt="example" src="https://i.ibb.co/r0tpq4t/image-64.png"/>}>
                            <Meta title="Restaurant"/>
                        </Card>,
                    </Col>
                    <Col span={8}>
                        <Card
                            hoverable
                            cover={<img alt="example" src="https://i.ibb.co/BqF0zdD/image-61.png"/>}>
                            <Meta title="Health Care"/>
                        </Card>,
                    </Col>
                </Row>
                
            </div>
        </div>
    )
}

export default BusinessCategory;