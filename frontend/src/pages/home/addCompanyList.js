import React from "react";
import { Form, Input, InputNumber, Button,AutoComplete, Upload, Select } from 'antd';
import { UserOutlined, InboxOutlined } from '@ant-design/icons';

const { Option } = Select;


function AddCompany() {
    return (
        <div className="block contactBlock">
            <div className="container-fluid">
                <div className="titleHolder">
                    <h2> List Out Your Company </h2>
                    <p> You can list your company free from here </p>
                </div>
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    >
                    <Form.Item
                        name="Company"
                        label="Company"
                        rules={[{ required: true, message: 'Please input your Company Name!' }]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Company" />
                    </Form.Item>
                    <Form.Item label="Dragger">
                        <Form.Item name="dragger" valuePropName="fileList"  noStyle>
                        <Upload.Dragger name="files" action="/upload.do">
                            <p className="ant-upload-drag-icon">
                            <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                        </Upload.Dragger>
                        </Form.Item>
                    </Form.Item>
                    <Form.Item
                        name="select"
                        label="Provinces"
                        hasFeedback
                        rules={[{ required: true, message: 'Please select your country!' }]}
                    >
                        <Select placeholder="Provinces">
                        <Option value="	Province No. 1">Province No. 1</Option>
                        <Option value="	Province No. 2">Province No. 2</Option>
                        <Option value="	Bagmati Province">Bagmati Province</Option>
                        <Option value="	Gandaki Province">Gandaki Province</Option>
                        <Option value="	Lumbini Province">Lumbini Province</Option>
                        <Option value="	Karnali Province">Karnali Province</Option>
                        <Option value="	Sudurpashchim Province">Sudurpashchim Province</Option>
                        </Select>
                    </Form.Item>
                    {/* <Form.Item
                        name="select"
                        label="Select"
                        hasFeedback
                        rules={[{ required: true, message: 'Please select your country!' }]}
                    >
                        <Select placeholder="state">
                        <Option value="china">China</Option>
                        <Option value="usa">U.S.A</Option>
                        </Select>
                    </Form.Item> */}
                    <Form.Item
                        name="address"
                        label="address"
                        rules={[{ required: true, message: 'Please input your address!' }]}
                    >
                        <Input style={{ width: '100%' }} />
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        type="number"
                        label="Phone Number"
                        rules={[{ required: true, message: 'Please input your phone number!' }]}
                        
                    >
                        <InputNumber  style={{ width: '100%' }}/>
                        
                    </Form.Item>
                    <Form.Item
                        name="mobile"
                        type="number"
                        label="mobile"
                        rules={[{ required: true, message: 'Please input your phone number!' }]}
                    >
                        <InputNumber style={{ width: '100%' }} />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="website"
                        label="Website"
                        rules={[{ required: true, message: 'Please input website!' }]}
                    >
                        <AutoComplete  placeholder="website">
                        <Input />
                        </AutoComplete>
                    </Form.Item>
                    <Form.Item name={['user', 'introduction']} label="Introduction">
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                        Add Company
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default AddCompany;