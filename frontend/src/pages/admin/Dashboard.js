import React from 'react';
import Sidebar from "./Sidebar.js";
import "./Dashboard.css"
import AppHeader from '../common/header.js';
import AppFooter from '../common/footer.js';

const Dashboard = () => {
    return (
        <div className="dashboard">
            <AppHeader/>
            <Sidebar />
            {/* <div className='dashboardContainer'>
                <Typography component="h1">Dashboard</Typography>
                <div className="dashboardSummary">
                    <div className="dashboardSummaryBox2">
                        <Link to="/admin/companys">
                            <p>Company</p>
                            <p>{companys && companys.length}</p>
                        </Link>
                        <Link to="/admin/users">
                        <p>Users</p>
                        <p>{users && users.length}</p>
                        </Link>
                    </div>
                </div>
            </div> */}
            <AppFooter/> 
        </div>
    )
}

export default Dashboard
