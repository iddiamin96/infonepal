// import React, { Fragment, useEffect } from "react";
// import { DataGrid } from "@material-ui/data-grid";
// // import "./productList.css";
// import { useSelector, useDispatch } from "react-redux";
// import { Link } from "react-router-dom";
// import { useAlert } from "react-alert";
// import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
// import { Form, Input, Button,AutoComplete, Upload, Select } from 'antd'
// import { clearErrors, deleteUser, getAllUsers } from "../../../actions/userAction";
// import { DELETE_USER_RESET } from "../../../constants/userConstants";
// import Sidebar from "../Sidebar";

// const UsersList = () => {
//   const dispatch = useDispatch();

//   const alert = useAlert();

//   const { error, users } = useSelector((state) => state.getAllUser);

//   const {
//     error: deleteError,
//     isDeleted,
//     message,
//   } = useSelector((state) => state.profile);

//   const deleteUserHandler = (id) => {
//     dispatch(deleteUser(id));
//   };

//   useEffect(() => {
//     if (error) {
//       alert.error(error);
//       dispatch(clearErrors());
//     }

//     if (deleteError) {
//       alert.error(deleteError);
//       dispatch(clearErrors());
//     }

//     if (isDeleted) {
//       alert.success(message);
//     //   history.push("/admin/users");
//     window.location.href = "/admin/users";
//       dispatch({ type: DELETE_USER_RESET });
//     }

//     dispatch(getAllUsers());
//   }, [dispatch, alert, error, deleteError, isDeleted, message]);

//   const columns = [
//     { field: "id", headerName: "User ID", minWidth: 180, flex: 0.8 },

//     {
//       field: "email",
//       headerName: "Email",
//       minWidth: 200,
//       flex: 1,
//     },
//     {
//       field: "name",
//       headerName: "Name",
//       minWidth: 150,
//       flex: 0.5,
//     },

//     {
//       field: "role",
//       headerName: "Role",
//       type: "number",
//       minWidth: 150,
//       flex: 0.3,
//       cellClassName: (params) => {
//         return params.getValue(params.id, "role") === "admin"
//           ? "greenColor"
//           : "redColor";
//       },
//     },

//     {
//       field: "actions",
//       flex: 0.3,
//       headerName: "Actions",
//       minWidth: 150,
//       type: "number",
//       sortable: false,
//       renderCell: (params) => {
//         return (
//           <Fragment>
//             <Link to={`/api/v1/admin/users/${params.getValue(params.id, "id")}`}>
//             <EditOutlined />
//             </Link>

//             <Button
//               onClick={() =>
//                 deleteUserHandler(params.getValue(params.id, "id"))
//               }
//             >
//               <DeleteOutlined />
//             </Button>
//           </Fragment>
//         );
//       },
//     },
//   ];

//   const rows = [];

//   users &&
//     users.forEach((item) => {
//       rows.push({
//         id: item._id,
//         role: item.role,
//         email: item.email,
//         name: item.name,
//       });
//     });

//   return (
//     <Fragment>
//       {/* <MetaData title={`ALL USERS - Admin`} /> */}

//       <div className="dashboard">
//         <Sidebar />
//         <div className="productListContainer">
//           <h1 id="productListHeading">ALL USERS</h1>

//           <DataGrid
//             rows={rows}
//             columns={columns}
//             pageSize={10}
//             disableSelectionOnClick
//             className="productListTable"
//             autoHeight
//           />
//         </div>
//       </div>
//     </Fragment>
//   );
// };

// export default UsersList


import React from 'react'

const UsersList = () => {
    return (
        <div>
            iddi
        </div>
    )
}

export default UsersList


// import React, { Fragment, useEffect } from "react";
// import { DataGrid } from "@material-ui/data-grid";
// import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
// import { Form, Input, Button,AutoComplete, Upload, Select } from 'antd'
// import "./UsersList.css";
// import { useSelector, useDispatch } from "react-redux";
// import {
//   clearErrors,
//   getAdminUsers,
//   deleteUsers,
// } from "../../actions/UsersAction";
// import { Link } from "react-router-dom";
// import { useAlert } from "react-alert";
// import MetaData from "../common/metaData";
// import { clearErrors, deleteUser, getAllUsers } from "../../../actions/userAction";
// import { DELETE_USER_RESET } from "../../../constants/userConstants";
// import Sidebar from "../Sidebar";


// const UsersLists = () => {
//     const dispatch = useDispatch();

//     const alert = useAlert();

//     const { error, Userslist } = useSelector((state) => state.Userslists);

//     const { error: deleteError, isDeleted } = useSelector(
//         (state) => state.Userslists
//     );

//     const deleteUsersHandler = (id) => {
//         dispatch(deleteUsers(id));
//       };

//     useEffect(() => {
//         if (error) {
//             alert.error(error);
//             dispatch(clearErrors());
//         }
    
//         if (deleteError) {
//         alert.error(deleteError);
//         dispatch(clearErrors());
//         }
    
//         if (isDeleted) {
//         alert.success("Users Deleted Successfully");
//         window.location.href = "/admin/Userslists";
        
//         dispatch({ type: DELETE_USER_RESET });
//         }
    
//         dispatch(getAdminUsers());
//       }, [dispatch,alert, error, deleteError, isDeleted]);

//     const columns = [
//         { field: "id", headerName: "Users ID", minWidth: 200, flex: 0.5 },

//         {
//         field: "name",
//         headerName: "Name",
//         minWidth: 350,
//         flex: 0.3,
//         },
//         {
//         field: "category",
//         headerName: "Category",
//         minWidth: 350,
//         flex: 0.3,
//         },
//         {
//         field: "state",
//         headerName: "State",
//         minWidth: 350,
//         flex: 0.3,
//         },
//         {
//         field: "actions",
//         flex: 0.3,
//         headerName: "Actions",
//         minWidth: 150,
//         type: "number",
//         sortable: false,
//         renderCell: (params) => {
//             return (
//             <Fragment>
//                 <Link to={`/api/v1/admin/Userslist/${params.getValue(params.id, "id")}`}>
//                 <EditOutlined />
//                 </Link>

//                 <Button
//                 onClick={() =>
//                     deleteUsersHandler(params.getValue(params.id, "id"))
//                 }
//                 >
//                 <DeleteOutlined />
//                 </Button>
//             </Fragment>
//             );
//         },
//         },
//     ];

//     const rows = [];

//     Userslist &&
//         Userslist.forEach((item) => {
//             rows.push({
//             id: item._id,
//             name: item.name,
//             category: item.category,
//             state: item.state,
//             });
//         });

//     return (
//         <Fragment>
//             <div className="dashboard">
//             {/* <MetaData title={`ALL PRODUCTS - Admin`} /> */}
//             <Sidebar />
//                 <div className="UsersListContainer">
//                 <h1 id="UsersListHeading">ALL UsersS</h1>

//                 <DataGrid
//                     rows={rows}
//                     columns={columns}
//                     pageSize={10}
//                     disableSelectionOnClick
//                     className="UsersListTable"
//                     autoHeight
//                 />
//                 </div>
//             </div>
//         </Fragment>
//     )
// }

// export default UsersLists
