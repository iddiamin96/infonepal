import React, { Fragment, useEffect } from "react";
import { DataGrid } from "@material-ui/data-grid";
import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
import { Form, Input, Button,AutoComplete, Upload, Select } from 'antd'
import "./CompanyList.css";
import { useSelector, useDispatch } from "react-redux";
import {
  clearErrors,
  getAdminCompany,
  deleteCompany,
} from "../../actions/companyAction";
import { Link } from "react-router-dom";
import { useAlert } from "react-alert";
import MetaData from "../common/metaData";
import Sidebar from "./Sidebar";
import { DELETE_COMPANY_RESET } from "../../constants/companyConstants";

const CompanyLists = () => {
    const dispatch = useDispatch();

    const alert = useAlert();

    const { error, companylist } = useSelector((state) => state.companylists);

    const { error: deleteError, isDeleted } = useSelector(
        (state) => state.companylists
    );

    const deleteCompanyHandler = (id) => {
        dispatch(deleteCompany(id));
      };

    useEffect(() => {
        if (error) {
            alert.error(error);
            dispatch(clearErrors());
        }
    
        if (deleteError) {
        alert.error(deleteError);
        dispatch(clearErrors());
        }
    
        if (isDeleted) {
        alert.success("Company Deleted Successfully");
        window.location.href = "/admin/companylists";
        
        dispatch({ type: DELETE_COMPANY_RESET });
        }
    
        dispatch(getAdminCompany());
      }, [dispatch,alert, error, deleteError, isDeleted]);

    const columns = [
        { field: "id", headerName: "Company ID", minWidth: 200, flex: 0.5 },

        {
        field: "name",
        headerName: "Name",
        minWidth: 350,
        flex: 0.3,
        },
        {
        field: "category",
        headerName: "Category",
        minWidth: 350,
        flex: 0.3,
        },
        {
        field: "state",
        headerName: "State",
        minWidth: 350,
        flex: 0.3,
        },
        {
        field: "actions",
        flex: 0.3,
        headerName: "Actions",
        minWidth: 150,
        type: "number",
        sortable: false,
        renderCell: (params) => {
            return (
            <Fragment>
                <Link to={`/api/v1/admin/companylist/${params.getValue(params.id, "id")}`}>
                <EditOutlined />
                </Link>

                <Button
                onClick={() =>
                    deleteCompanyHandler(params.getValue(params.id, "id"))
                }
                >
                <DeleteOutlined />
                </Button>
            </Fragment>
            );
        },
        },
    ];

    const rows = [];

    companylist &&
        companylist.forEach((item) => {
            rows.push({
            id: item._id,
            name: item.name,
            category: item.category,
            state: item.state,
            });
        });

    return (
        <Fragment>
            <div className="dashboard">
            {/* <MetaData title={`ALL PRODUCTS - Admin`} /> */}
            <Sidebar />
                <div className="companyListContainer">
                <h1 id="companyListHeading">ALL COMPANYS</h1>

                <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={10}
                    disableSelectionOnClick
                    className="companyListTable"
                    autoHeight
                />
                </div>
            </div>
        </Fragment>
    )
}

export default CompanyLists
