// import React, {Fragment,useState, useEffect} from 'react';
// import Sidebar from './Sidebar'
// import "./newCompany.css";
// import { Form, Input, Button,AutoComplete, Upload, Select } from 'antd';
// import { UserOutlined, InboxOutlined } from '@ant-design/icons';
// import { Link } from "react-router-dom";
// import { useDispatch, useSelector } from "react-redux";
// import { useAlert } from "react-alert";
// import { UPDATE_COMPANY_RESET } from "../../constants/companyConstants";
// import { clearErrors, updateCompany, getCompanyDetails } from '../../actions/companyAction';

// const UpdateCompany = ({match}) => {

//     const dispatch = useDispatch();
//     // const alert = useAlert();

//     const { error, companylist } = useSelector((state) => state.companyDetails);
//     const { error:updateError,loading, isUpdated } = useSelector((state) => state.companylist);

//     const [name, setName] = useState("");
//     const [logo, setLogo] = useState([]);
//     const [oldlogo, setOldLogo] = useState([]);
//     const [logoPreview, setLogoPreview] = useState([]);
//     const [category, setCategory] = useState("");
//     const [state, setState] = useState("");
//     const [phoneNumber, setPhoneNumber] = useState("");
//     const [mobileNumber, setMobileNumber] = useState("");
//     const [email, setEmail] = useState("");
//     const [websitelink, setWebsiteLink] = useState("");
//     const [mapEmbededCode, setMapEmbededCode] = useState("");
//     const [introduction, setCompanyDescription] = useState("");

//     const categories = [
//         "Shopping",
//         "Restaurant",
//         "Hotel & Resort",
//         "Tours &amp; Travel",
//         "Daily Needs",
//         "Beauty &amp; Grooming",
//         "Education &amp; Training",
//         "Health Care",
//         "Fitness",
//         "Insurance",
//         "Banking",
//         "Auto Mobile",
//         "Family Style",
//         "Fine Dining",
//         "Cafe Bistro",
//         "Fast Food"
//         ,"Restaurant Buffet"
//         ,"Barbecue"
//         ,"Bakery &amp; Cafe"
//         ,"Juice Bars"
//         ,"Bed And Breakfast"
//         ,"Casino Hotel"
//         ,"Condo Hotel"
//         ,"Guesthouse"
//         ,"Fashion"
//         ,"Computer &amp; Laptops "
//         ,"Jewelry"
//         ,"Kitchen Items"
//         ,"Bags"
//         ,"Footwear"
//         ,"Yoga"
//         ,"Flight",'Bus'
//         ,'Car Rentals'
//         ,'School &amp; College'
//         ,'Acting Classes'
//         ,'Art &amp; Craft Classes'
//         ,'Korean Language'
//         ,'Music Classes'
//         ,'Painting Classes'
//         ,'Photography Classes'
//         ,'Training Institutes'
//         ,'Children Hospitals'
//         ,'Ent Hospitals'
//         ,'Eye Hospitals'
//         ,'Maternity Hospital'
//         ,'Hospital'
//         ,'Ayurvedic '
//         ,'Veterinary '
//         ,'Beauty Parlours'
//         ,'Beauty Services'
//         ,'Bridal Makeup'
//         ,'Photo Studio'
//         ,'Salons'
//         ,'Spas'
//         ,'Health Insurance'
//         ,'General Insurance'
//         ,'Life Insurance'
//         ,'Manufacturers'
//         ,'Business Services'
//         ,'Professionals Services'
//         ,'Hardware &amp; Paint'
//         ,'Manufacturing /industries'
//         ,'Liquor Shops'
//         ,'Furniture &amp; Home Décor'
//         ,'Electronics'
//         ,'Hostels'
//         ,'Books &amp; Stationary'
//         ,'Books', 'Tailoring'
//         ,'Boutique'
//         ,'Toys'
//         ,'Tattoo'
//         ,'Tour &amp; Travelers '
//         ,'Electronics Repairing'
//         ,'Mobile &amp; Electronics'
//         ,'Graphics Design'
//         ,'Sports'
//         ,'Snooker'
//         ,'Futsal'
//         ,'Pharmacy'
//         ,'Catering'
//         ,'Meat Shop'
//         ,'Sunglasses &amp; Opticians' 
//         ,'Dj' , 'Music System '
//         ,'Milk &amp; Milk Products' 
//         ,'Boys Hostel'
//         ,'Girls Hostel'
//         ,'Art Shop'
//         ,'Machine Tool'
//         ,'Agriculture Farming'
//         ,'Online Publications'
//         ,'Liquors','Key House', 'Pan Pasal',' Hardware' ,'Tyre House','Paint','Plywood',
//         'Plastics &amp; Utensils' ,'Lounge &amp; Bar' ,'Auto Parts','Gifts','Grocery',
//         'Printing &amp; Designing' ,'Photocopy &amp; Print','Repairs &amp; Maintanance',
//         'Mobile Repairing','Electricians &amp; Plumbers','Computer Repairing',
//         'Second Hand Two Wheelers','Pet &amp; Pet Care','Furnitures','Sports Shop',
//         'Sweets Shops','Fruits &amp; Vegetables','Papers','Dry Cleaners','Aquarium House',
//         'Office Materials' ,'Home Décor','Cosmetics' ,'Petrol Pumps','ATM',
//         'Gas Station','Branches','Ev Charging Station','Tiles &amp; Marbles','Bath','Gym',
//         'New Cars','New Two Wheelers','Used Cars','Bicycle' ,'Helmet' ,'Opticians &amp; Sunglasses',
//         'Clinics','Glass','Modular Kitchens','Accounting &amp; Auditing','Legal Services',
//         'Advertisements','Taxation' ,'Chartered Accountants','Lawyers &amp; Advocates',
//         'Financial Advisors','Engineers','Printer','Utensils','Heavy Machines',
//         'Agricultural Machines','Iron {grill}','Aluminium' ,'Jipson','Hookah Shop','Watch' ,
//         'Lights','Study Abroad' ,'Tution Classes','Used Items','Mobile','Camera/ Lens',
//         'Sahakari Sanstha','Others'
//     ]
    
//     const provinces= [
//         'Province 1','Province 2','Province 3','Province 4','Province 5','Province 6','Province 7'
//     ]

//     const companylistId = match.params.id;

//     useEffect(() => {
//         if (companylist && companylist._id !== companylistId) {
//             dispatch(getCompanyDetails(companylistId));
//           } else {
//             setName(companylist.name);
//             setLogo(companylist.logo);
//             setCategory(companylist.category);
//             setState(companylist.state);
//             setPhoneNumber(companylist.phoneNumber);
//             setMobileNumber(companylist.mobileNumber);
//             setEmail(companylist.email);
//             setWebsiteLink(companylist.websitelink);
//             setMapEmbededCode(companylist.mapEmbededCode);
//             setCompanyDescription(companylist.introduction);
//             setOldLogo(companylist.logo);
//           }

//         if (error) {
//         //   alert.error(error);
//           dispatch(clearErrors());
//         }
    
//         if (isUpdated) {
//         //   alert.success("Product Created Successfully");
//         //   history.push("/admin/dashboard");
//             // window.location.href = "/admin/companylists";
//             dispatch({ type: UPDATE_COMPANY_RESET });
//         }
//       }, [dispatch, error, isUpdated, companylist, companylistId]);

//     const createCompanySubmitHandler = (e) => {
//         e.preventDefault();
    
//         const myForm = new FormData();
    
//         myForm.set("name", name);
//         myForm.set("category", category);
//         myForm.set("state", state);
//         myForm.set("phoneNumber", phoneNumber);
//         myForm.set("mobileNumber", mobileNumber);
//         myForm.set("email", email);
//         myForm.set("websitelink", websitelink);
//         myForm.set("mapEmbededCode", mapEmbededCode);
//         myForm.set("introduction", introduction);
    
//         logo.forEach((logo) => {
//           myForm.append("logos", logo);
//         });
//         dispatch(updateCompany(companylistId, myForm));
//     };
    
//         const createCompanyLogoChange = (e) => {
//         const files = Array.from(e.target.files);
    
//         setLogo([]);
//         setLogoPreview([]);
    
//         files.forEach((file) => {
//           const reader = new FileReader();
    
//           reader.onload = () => {
//             if (reader.readyState === 2) {
//               setLogoPreview((old) => [...old, reader.result]);
//               setLogo((old) => [...old, reader.result]);
//             }
//           };
    
//           reader.readAsDataURL(file);
//         });
//       };

//     return (
//         <Fragment>
//             <div className="dashboard">
//             <Sidebar />
//                 <div className="newCompanyContainer">
//                 <form
//                     className="createCompanyForm"
//                     encType="multipart/form-data"
//                     onSubmit={createCompanySubmitHandler}
//                 >
//                     <h1>Create Company</h1>
//                     <div>
//                     <input
//                         type="text"
//                         placeholder="Company Name"
//                         required
//                         value={name}
//                         onChange={(e) => setName(e.target.value)}
//                     />
//                     </div>
//                     <div id="createCompanyFormFile">
//                     <input
//                         type="file"
//                         name="logo"
//                         accept="image/*"
//                         onChange={createCompanyLogoChange}
//                         multiple
//                     />
//                     </div>

//                     <div id="createCompanyFormImage">
//                     {logoPreview.map((image, index) => (
//                         <img key={index} src={logo} alt="Company Preview" />
//                     ))}
//                     </div>
//                     <div>
//                     <select onChange={(e) => setCategory(e.target.value)}>
//                         <option value="">Choose Category</option>
//                         {categories.map((cate) => (
//                         <option key={cate} value={cate}>
//                             {cate}
//                         </option>
//                         ))}
//                     </select>
//                     </div>
//                     <div>
//                     <select onChange={(e) => setState(e.target.value)}>
//                         <option value="">Choose State</option>
//                         {provinces.map((state) => (
//                         <option key={state} value={state}>
//                             {state}
//                         </option>
//                         ))}
//                     </select>
//                     </div>
//                     <div>
//                     <input
//                         type="number"
//                         placeholder="Phone Number"
//                         required
//                         onChange={(e) => setPhoneNumber(e.target.value)}
//                     />
//                     </div>
//                     <div>
//                     <input
//                         type="number"
//                         placeholder="Mobile Number"
//                         required
//                         onChange={(e) => setMobileNumber(e.target.value)}
//                     />
//                     </div>

//                     <div>
//                     <input
//                         type="email"
//                         placeholder="Email"
//                         required
//                         onChange={(e) => setEmail(e.target.value)}
//                     />
//                     </div>
//                     <div>
//                     <input
//                         type="text"
//                         placeholder="Website Url"
//                         required
//                         value={websitelink}
//                         onChange={(e) => setWebsiteLink(e.target.value)}
//                     />
//                     </div>
//                     <div>
//                         <input
//                         type="text"
//                         placeholder="Embeded Map URL"
//                         required
//                         value={mapEmbededCode}
//                         onChange={(e) => setMapEmbededCode(e.target.value)}
//                     />
//                     </div>

//                     <div>
//                     <textarea
//                         placeholder="Company Description"
//                         value={introduction}
//                         onChange={(e) => setCompanyDescription(e.target.value)}
//                         cols="30"
//                         rows="1"
//                     ></textarea>
//                     </div>
//                     <input type="submit" value="CREATE" id="createCompanyBtn" />

//                     <Button
//                     id="createCompanyBtn"
//                     type="submit"
//                     disabled={loading ? true : false}
//                     >
//                     Create
//                     </Button>
//                 </form>
//                 </div>
//             </div>
//         </Fragment>
//     )
// }

// export default UpdateCompany
import React from 'react'

const UpdateCompany = () => {
    return (
        <div>
            iddi
        </div>
    )
}

export default UpdateCompany
