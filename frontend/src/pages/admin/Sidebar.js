import React from "react";
import "./sidebar.css";
import { Link } from "react-router-dom";
import { TreeView, TreeItem } from "@material-ui/lab";
import { MinusCircleOutlined,PlusCircleOutlined,StepBackwardOutlined,StepForwardOutlined,UserOutlined,DashOutlined  } from '@ant-design/icons'

const Sidebar = () => {
    return (
        <div className="sidebar">
      <Link to="/">
      <img alt="Dashboard" src="https://i.ibb.co/mGLv9gD/info-NEPAL.jpg"/>
      </Link>
      {/* <Link to="/admin/dashboard">
        <p>
        <DashOutlined /> Dashboard
        </p>
      </Link> */}
      {/* <Link>
        <TreeView
          defaultCollapseIcon={<MinusCircleOutlined />}
          defaultExpandIcon={<PlusCircleOutlined />}
        >
          <TreeItem nodeId="1" label="Companys">
            <Link to="/admin/companys">
              <TreeItem nodeId="2" label="All" icon={<StepBackwardOutlined />} />
            </Link>

            <Link to="/admin/company">
              <TreeItem nodeId="3" label="Create" icon={<StepBackwardOutlined />} />
            </Link>
          </TreeItem>
        </TreeView>
      </Link> */}
    <Link to="/admin/companylists">
        <p>
        <UserOutlined /> Companys
        </p>
    </Link>
    <Link to="/admin/companylist">
        <p>
        <PlusCircleOutlined />
        Company
        </p>
    </Link>
      
      <Link to="/admin/users">
        <p>
        <UserOutlined /> Users
        </p>
      </Link>
      <Link to="/admin/reviews">
        <p>
        <PlusCircleOutlined />
          Reviews
        </p>
      </Link>
    </div>
  );
};


export default Sidebar
