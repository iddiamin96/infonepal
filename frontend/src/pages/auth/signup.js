import React, {useState, useEffect} from 'react';
import { Form, Input, Button,AutoComplete, Upload, Select } from 'antd';
import { UserOutlined, InboxOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clearErrors, login, register } from "../../actions/userAction";
import './signup.css';
import { useAlert } from "react-alert";

const { Option } = Select;

function SignUp() {

    // const dispatch = useDispatch();
    // const { error, isAuthenticated } = useSelector(
    //     (state) => state.user
    //   ); 

    // const [user, setUser] = useState({
    //     name: "",
    //     email: "",
    //     password: "",
    // });

    // useEffect(() => {
    //     if (error) {
    //       // alert.error(error);
    //       dispatch(clearErrors());
    //     }
    
    //     if (isAuthenticated) {
    //       window.location.href = "/account";
    //     }
    //   }, [dispatch, error, isAuthenticated]);

    // const registerSubmit = (e)=> {
    //     e.preventDefault();

    //     const myForm = new FormData();

    //     myForm.set("name", name);
    //     myForm.set("email", email);
    //     myForm.set("password", password);
    //     myForm.set("avatar", avatar);
    //     dispatchEvent(register(myForm));
    // }

    // const registerDataChange = (e) => {
    //     if (e.target.name === "avatar" ){
    //         const reader = new FileReader();

    //         reader.onload = () => {
    //             if (reader.readyState === 2){
    //                 setAvatarPreview(reader.result);
    //                 setAvatar(reader.result);

    //             }
    //         };

    //         reader.readAsDataURL(e.target.files[0]);
    //     }else{
    //         setUser({...user, [e.target.name]: e.target.value });
    //     }
    // }



    // const { name,email, password } = user;

    const dispatch = useDispatch();
    const alert = useAlert();

  
    const { error, loading, isAuthenticated } = useSelector(
      (state) => state.user
    );

  
    const [loginEmail, setLoginEmail] = useState("");
    const [loginPassword, setLoginPassword] = useState("");
  
    const [user, setUser] = useState({
      name: "",
      email: "",
      password: "",
    });
  
    const { name, email, password } = user;
  
  
    const registerSubmit = (e) => {
      e.preventDefault();
  
      const myForm = new FormData();
  
      myForm.set("name", name);
      myForm.set("email", email);
      myForm.set("password", password);
      myForm.set("avatar", avatar);
      dispatch(register(myForm));
    };
  
    const registerDataChange = (e) => {
      if (e.target.name === "avatar") {
        const reader = new FileReader();
  
        reader.onload = () => {
          if (reader.readyState === 2) {
            setAvatarPreview(reader.result);
            setAvatar(reader.result);
          }
        };
  
        reader.readAsDataURL(e.target.files[0]);
      } else {
        setUser({ ...user, [e.target.name]: e.target.value });
      }
    };
    
    useEffect(() => {
      if (error) {
        alert.error(error);
        dispatch(clearErrors());
      }
  
      if (isAuthenticated) {
        window.location.href = "/account";
      }
    }, [dispatch,alert, error, isAuthenticated]);

    const [avatar, setAvatar] = useState();
    const [avatarPreview, setAvatarPreview] = useState("/logo512.png");


    return (
        <div className="block contactBlock">
            <div className="container-fluid">
                <div className="titleHolder">
                  <h1 className="header" style={{textAlign: "center"}}><b>SignUp to Info Nepal</b></h1>
                  <p> SignUp Here</p>
                </div>
                {/* <Form
                    name="normal_register"
                    className="register-form"
                    initialValues={{ remember: true }}
                    onSubmit={registerSubmit}
                    >
                    <Form.Item
                        name="Company"
                        label="Company Name"
                        rules={[{ required: true, message: 'Please input your Company Name!' }]}
                    >
                        <Input 
                            prefix={<UserOutlined className="site-form-item-icon" />} 
                            placeholder="Company"
                            type="text"
                            placeholder="Company Name"
                            required
                            value={name}
                            onChange={registerDataChange}
                            />
                    </Form.Item>
                    <Form.Item id="registerImage">
                        <img src={avatarPreview} alt="Avatar preview" />
                            <Input 
                                type="file"
                                name="avatar"
                                accept="image/*"
                                onChange={registerDataChange}
                                />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                        ]}
                    >
                            <Input 
                                type="email"
                                placeholder="Email"
                                required
                                name="email"
                                value={email}
                                onChange={registerDataChange}
                            />
                    </Form.Item>
                    <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password.',
                                },
                            ]}
                        >
                            <Input.Password 
                                type="password"
                                placeholder="Password"
                                required
                                value={password}
                                onChange={registerDataChange}
                            />
                        </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                        Sign Up
                        </Button>
                        <Button type="primary"><Link to="/login">Login</Link></Button>

                    </Form.Item>
                </Form> */}
                <form
                className="signUpForm"
                encType="multipart/form-data"
                onSubmit={registerSubmit}
              >
                <div className="signUpName">
                  <input
                    type="text"
                    placeholder="Name"
                    required
                    name="name"
                    value={name}
                    onChange={registerDataChange}
                  />
                </div>
                <div className="signUpEmail">
                  <input
                    type="email"
                    placeholder="Email"
                    required
                    name="email"
                    value={email}
                    onChange={registerDataChange}
                  />
                </div>
                <div className="signUpPassword">
                  <input
                    type="password"
                    placeholder="Password"
                    required
                    name="password"
                    value={password}
                    onChange={registerDataChange}
                  />
                </div>
                <div id="registerImage">
                  <img src={avatarPreview} alt="Avatar Preview" />
                  <input
                    type="file"
                    name="avatar"
                    accept="image/*"
                    onChange={registerDataChange}
                  />
                </div>
                <input type="submit" value="Register" className="signUpBtn" />
              </form>
            </div>
        </div>
  )
}

export default SignUp
