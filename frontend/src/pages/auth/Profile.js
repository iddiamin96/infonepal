import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import MetaData from '../common/metaData';
import { Link } from "react-router-dom";
import { loadUser } from '../../actions/userAction';
import "./Profile.css";
import AppHeader from '../common/header';
import AppFooter from '../common/footer';


const Profile = () => {

  const dispatch = useDispatch();

    const {user, isAuthenticated} = useSelector((state) => state.user);

    useEffect(() => {
      dispatch(loadUser());
    }, [dispatch])

    // useEffect(() => {
    //     if (isAuthenticated === false) {
    //       window.location.href = "/login";
    //     }
    // }, [ isAuthenticated]);

    return (
        <Fragment>
          <AppHeader/>
          {typeof user !== 'undefined' ? (
           <MetaData title={`${user.name}'s Profile`} />
          ): null}
          <div className="profileContainer">
            <div>
              <h1>My Profile</h1>
                {typeof user !== 'undefined' ? (
                  <img src={user.avatar[0].url} alt={user.name} />
                ): null}
              <Link to="/profile/update">Edit Profile</Link>
            </div>
            <div>
              <div>
                {typeof user !== 'undefined' ? (
                  <>
                  <h4>Full Name</h4>
                  <p>{user.name}</p>
                  </>
                ): null}
                
              </div>
              <div>
                <h4>Email</h4>
                  {typeof user !== 'undefined' ? (
                  <p>{user.email}</p>
                  ): null}
              </div>
              <div>
                <h4>Joined On</h4>
                  {typeof user !== 'undefined' ? (
                    <p>{String(user.createdAt).substr(0, 10)}</p>
                  ): null}
              </div>

              <div>
                <Link to="/password/update">Change Password</Link>
              </div>
            </div>
          </div>
          <AppFooter/> 
        </Fragment>
    )
}

export default Profile

