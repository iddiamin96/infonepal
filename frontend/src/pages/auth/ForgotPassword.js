import React, {useState, useEffect, Fragment} from "react";
import { Layout, Form, Input, Button, Row, Col, } from "antd";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clearErrors, forgotPassword } from "../../actions/userAction";
import {useAlert} from "react-alert";
import "./Profile";
import "./ForgotPassword.css";
import MetaData from "../common/metaData";
import { UPDATE_PROFILE_RESET } from "../../constants/userConstants";


const ForgotPassword = () => {

    const dispatch = useDispatch();
    // const alert = useAlert();
    const { user } = useSelector(
        (state) => state.user
      ); 
    
    const { error, message } = useSelector((state) => state.forgotPassword);

    const [email, setEmail] = useState("");

    const forgotPasswordSubmit = (e) => {
        e.preventDefault();
    
        const myForm = new FormData();

        myForm.set("email", email);
        dispatch(forgotPassword(myForm));
      };

    useEffect(() => {

    if (error) {
        dispatch(clearErrors());
    }

    }, [dispatch, error]);

    return (
    <Fragment>
        <MetaData title="Forgot Password" />
        <div className="forgotPasswordContainer">
        <div className="forgotPasswordBox">
            <h2 className="forgotPasswordHeading">Forgot Password</h2>
            <form
            className="forgotPasswordForm"
            onSubmit={forgotPasswordSubmit}
            >
            <div className="forgotPasswordEmail">
                <input
                type="email"
                placeholder="Email"
                required
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <input
                type="submit"
                value="Send"
                className="forgotPasswordBtn"
            />
            </form>
        </div>
        </div>
    </Fragment>
    )
}

export default ForgotPassword
