import React, {useState, useEffect, Fragment} from "react";
import { Layout, Form, Input, Button, Row, Col, } from "antd";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clearErrors, resetPassword } from "../../actions/userAction";
import {useAlert} from "react-alert";
import "./Profile";
import "./resetPassword.css";
import MetaData from "../common/metaData";
import { UPDATE_PASSWORD_RESET } from "../../constants/userConstants";

const ResetPassword = ({match}) => {
    const dispatch = useDispatch();
    // const alert = useAlert(); 
    
    const { error, success } = useSelector((state) => state.forgotPassword);
    
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const resetPasswordSubmit = (e) => {
      e.preventDefault();
  
      const myForm = new FormData();
  
      myForm.set("password", password);
      myForm.set("confirmPassword", confirmPassword);
      dispatch(resetPassword(match.params.token,myForm));
    };
    
    useEffect(() => {
      if (error) {
        dispatch(clearErrors());
      }
  
      if (success) {
        window.location.href = "/login";
      }
    }, [dispatch, error, success]);
    return (
        <Fragment>
          <MetaData title="Update Password" />
          <div className="resetPasswordContainer">
            <div className="resetPasswordBox">
              <h2 className="resetPasswordHeading">Update Profile</h2>
              <form
                className="resetPasswordForm"
                onSubmit={resetPasswordSubmit}
              >
                <div className="loginPassword">
                  <input
                    type="password"
                    placeholder="New Password"
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                </div>
                <div className="loginPassword">
                  <input
                    type="password"
                    placeholder="Confirm Password"
                    required
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                />
                </div>
                <input
                  type="submit"
                  value="Update"
                  className="resetPasswordBtn"
                />
              </form>
            </div>
          </div>
        </Fragment>
    )
}

export default ResetPassword
   