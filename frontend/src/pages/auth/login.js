import React, {useState, useEffect} from "react";
import { Layout, Form, Input, Button, Row, Col,Alert } from "antd";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clearErrors, login, register } from "../../actions/userAction";
import {useAlert} from "react-alert";
import "./Profile";


const { Content } = Layout;

function Login(){

    const dispatch = useDispatch();
    // const alert = useAlert();
    const { error, isAuthenticated } = useSelector(
        (state) => state.user
      ); 


    const [loginEmail, setLoginEmail] = useState("");
    const [loginPassword, setLoginPassword] = useState("");

    const loginSubmit = (e)=> {
        e.preventDefault();
        dispatch(login(loginEmail, loginPassword));
    }

    useEffect(() => {
        if (error) {
          // alert.error(error);
          <Alert message="Error" type="error" />
          dispatch(clearErrors());
        }
    
        if (isAuthenticated) {
          window.location.href = "/account";
        }
      }, [dispatch, error, isAuthenticated]);

    return (

        <>
          <div className="LoginSignUpContainer">
            <div className="LoginSignUpBox">
              <form className="loginForm" onSubmit={loginSubmit}>
                <div className="loginEmail">
                  <input
                    type="email"
                    placeholder="Email"
                    required
                    value={loginEmail}
                    onChange={(e) => setLoginEmail(e.target.value)}
                  />
                </div>
                <div className="loginPassword">
                  <input
                    type="password"
                    placeholder="Password"
                    required
                    value={loginPassword}
                    onChange={(e) => setLoginPassword(e.target.value)}
                  />
                </div>
                <Link to="/password/forgot">Forget Password ?</Link>
                <input type="submit" value="Login" className="loginBtn" />
                <Button type="primary"><Link to="/signup">signup</Link></Button>

              </form>
            </div>
          </div>
        </>
    )
}

export default Login;
