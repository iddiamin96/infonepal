import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { companyReducer, companyDetailReducer, newReviewReducer,newCompanyReducer, companysReducer } from "./reducers/companyReducer";
import { allUsersReducer, forgotPasswordReducer, profileReducer, userDetailsReducer, userReducer } from "./reducers/userReducer";

const reducer = combineReducers({
    companylists:companyReducer,
    companyDetails:companyDetailReducer,
    user: userReducer,
    profile: profileReducer,
    forgotPassword: forgotPasswordReducer,
    newReview: newReviewReducer,
    newCompany: newCompanyReducer,
    companys:companysReducer,
    allUsers: allUsersReducer,
    userDetails:userDetailsReducer,
});

let initialState={};

const middleware = [thunk];

const store = createStore(
    reducer, 
    initialState, 
    composeWithDevTools(applyMiddleware(...middleware))
    );

export default store;
