import {ALL_COMPANY_REQUEST, ALL_COMPANY_SUCCESS,ALL_COMPANY_FAIL,
        ADMIN_COMPANY_REQUEST,
        ADMIN_COMPANY_SUCCESS,
        ADMIN_COMPANY_FAIL,
        ALL_COMPANY_DETAILS_REQUEST, ALL_COMPANY_DETAILS_SUCCESS,ALL_COMPANY_DETAILS_FAIL,CLEAR_ERRORS,
        NEW_REVIEW_REQUEST,
        NEW_REVIEW_SUCCESS,
        NEW_REVIEW_FAIL,
        NEW_REVIEW_RESET,
        NEW_COMPANY_REQUEST,
        NEW_COMPANY_SUCCESS,
        NEW_COMPANY_FAIL,
        NEW_COMPANY_RESET,
        UPDATE_COMPANY_REQUEST,
        UPDATE_COMPANY_SUCCESS,
        UPDATE_COMPANY_FAIL,
        UPDATE_COMPANY_RESET,
        DELETE_COMPANY_REQUEST,
        DELETE_COMPANY_SUCCESS,
        DELETE_COMPANY_FAIL,
        DELETE_COMPANY_RESET,
        COMPANY_DETAILS_REQUEST,
        COMPANY_DETAILS_FAIL,
        COMPANY_DETAILS_SUCCESS,
        ALL_REVIEW_REQUEST,
        ALL_REVIEW_SUCCESS,
        ALL_REVIEW_FAIL,
        DELETE_REVIEW_REQUEST,
        DELETE_REVIEW_SUCCESS,
        DELETE_REVIEW_FAIL,
        DELETE_REVIEW_RESET,} from "../constants/companyConstants"

export const companyReducer = (state = { companylist:[]}, action)=> {
    switch(action.type){
        case ALL_COMPANY_REQUEST:
            case ADMIN_COMPANY_REQUEST:
            return {
                loading:true,
                companylist:[]
            }
        case ALL_COMPANY_SUCCESS:
            return{
                loading:false,
                companylist: action.payload.companylist,
                companylistsCount:action.payload.companylistsCount,
            };
        case ADMIN_COMPANY_SUCCESS:
            return{
                loading:false,
                companylist: action.payload,
            };
        case ALL_COMPANY_FAIL:
            case ADMIN_COMPANY_FAIL:
            return{
                loading:false,
                error:action.payload,
            };
        case CLEAR_ERRORS:
            return{
                ...state,
                error:null,
            };
        default:
            return state;
    }
};

export const companyDetailReducer = (state = { companylist: {}}, action)=> {
    switch(action.type){
        case ALL_COMPANY_DETAILS_REQUEST:
            return {
                loading:true,
                ...state,
            }
        case ALL_COMPANY_DETAILS_SUCCESS:
            return{
                loading:false,
                companylist: action.payload,
                
            };
        case ALL_COMPANY_DETAILS_FAIL:
            return{
                loading:false,
                error:action.payload,
            };
        case CLEAR_ERRORS:
            return{
                ...state,
                error:null,
            };
        default:
            return state;
    }
};

export const newCompanyReducer = (state = {companylist: {} }, action)=> {
    switch(action.type){
        case NEW_COMPANY_REQUEST:
            return {
                loading:true,
                ...state,
            }
        case NEW_COMPANY_SUCCESS:
            return{
                loading:false,
                success: action.payload.success,
                companylist: action.payload.companylist,
                
            };
        case NEW_COMPANY_FAIL:
            return{
                ...state,
                loading:false,
                error:action.payload,
            };
        case NEW_COMPANY_RESET:
            return{
                ...state,
                success:false,
            };
        case CLEAR_ERRORS:
            return{
                ...state,
                error:null,
            };
        default:
            return state;
    }
};

export const companysReducer = (state = { }, action)=> {
    switch(action.type){
        case DELETE_COMPANY_REQUEST:
        case UPDATE_COMPANY_REQUEST:
            return {
                loading:true,
                ...state,
            }
        case DELETE_COMPANY_SUCCESS:
            return{
                ...state,
                loading:false,
                isDeleted: action.payload,
                
            };
        case UPDATE_COMPANY_SUCCESS:
            return{
                ...state,
                loading:false,
                isUpdated: action.payload,
                
            };
        
        case DELETE_COMPANY_FAIL:
        case UPDATE_COMPANY_FAIL:
            return{
                ...state,
                loading:false,
                error:action.payload,
            };
        case DELETE_COMPANY_RESET:
            return{
                ...state,
                isDeleted:false,
            };
        case UPDATE_COMPANY_RESET:
            return{
                ...state,
                isUpdated:false,
            };
        case CLEAR_ERRORS:
            return{
                ...state,
                error:null,
            };
        default:
            return state;
    }
};

export const newReviewReducer = (state = { }, action)=> {
    switch(action.type){
        case NEW_REVIEW_REQUEST:
            return {
                loading:true,
                ...state,
            }
        case NEW_REVIEW_SUCCESS:
            return{
                loading:false,
                success: action.payload,
                
            };
        case NEW_REVIEW_FAIL:
            return{
                ...state,
                loading:false,
                error:action.payload,
            };
        case NEW_REVIEW_RESET:
            return{
                ...state,
                success:false,
            };
        case CLEAR_ERRORS:
            return{
                ...state,
                error:null,
            };
        default:
            return state;
    }
};