import axios from "axios";
import {ALL_COMPANY_REQUEST, ALL_COMPANY_SUCCESS,ALL_COMPANY_FAIL,CLEAR_ERRORS,
        ALL_COMPANY_DETAILS_REQUEST, ALL_COMPANY_DETAILS_SUCCESS,ALL_COMPANY_DETAILS_FAIL,
        NEW_REVIEW_REQUEST,
        NEW_REVIEW_SUCCESS,
        NEW_REVIEW_FAIL,
        NEW_REVIEW_RESET,
        ALL_REVIEW_REQUEST,
        ALL_REVIEW_SUCCESS,
        ALL_REVIEW_FAIL,
        DELETE_REVIEW_REQUEST,
        DELETE_REVIEW_SUCCESS,
        DELETE_REVIEW_FAIL,
        DELETE_REVIEW_RESET,
        NEW_COMPANY_REQUEST,
        NEW_COMPANY_SUCCESS,
        NEW_COMPANY_FAIL,
        NEW_COMPANY_RESET,
        UPDATE_COMPANY_REQUEST,
        UPDATE_COMPANY_SUCCESS,
        UPDATE_COMPANY_FAIL,
        UPDATE_COMPANY_RESET,
        DELETE_COMPANY_REQUEST,
        DELETE_COMPANY_SUCCESS,
        DELETE_COMPANY_FAIL,
        DELETE_COMPANY_RESET,
        COMPANY_DETAILS_REQUEST,
        COMPANY_DETAILS_FAIL,
        COMPANY_DETAILS_SUCCESS,
        ADMIN_COMPANY_REQUEST,
        ADMIN_COMPANY_SUCCESS,
        ADMIN_COMPANY_FAIL,} from "../constants/companyConstants";

export const getCompany = () => async (dispatch) => {
    try{
        dispatch({
            type: ALL_COMPANY_REQUEST
        });
        const {data} = await axios.get("http://localhost:5000/api/v1/admin/companylist")
        dispatch({
            type:ALL_COMPANY_SUCCESS,
            payload: data,
        })
    }catch (error) {
        dispatch({
            type: ALL_COMPANY_FAIL,
            payload:error.response.data.message,
        })
    }
}


//add Company
export const createCompany = (companyData) => async (dispatch) => {
    try{
        dispatch({
            type: NEW_COMPANY_REQUEST
        });

        const config = {
            headers: { "Content-Type": "application/json" },
          };

        const {data} = await axios.post(`http://localhost:5000/api/v1/admin/companylist`, companyData, config);

        dispatch({
            type: NEW_COMPANY_SUCCESS,
            payload: data,
        });
    }catch (error) {
        dispatch({
            type: NEW_COMPANY_FAIL,
            payload:error.response.data.message,
        })
    }
}

//Update Company
export const updateCompany = (id,companyData) => async (dispatch) => {
    try{
        dispatch({
            type: UPDATE_COMPANY_REQUEST
        });

        const config = {
            headers: { "Content-Type": "application/json" },
          };

        const {data} = await axios.put(`/api/v1/admin/companylist/${id}`, companyData, config);

        dispatch({
            type: UPDATE_COMPANY_SUCCESS,
            payload: data.success,
        });
    }catch (error) {
        dispatch({
            type: UPDATE_COMPANY_FAIL,
            payload:error.response.data.message,
        })
    }
}

//GET ALL COMPANY (ADMIN)
export const getAdminCompany = () => async (dispatch) => {
    try{
        dispatch({
            type: ADMIN_COMPANY_REQUEST
        });
        const {data} = await axios.get("http://localhost:5000/api/v1/admin/companylist")
        dispatch({
            type:ADMIN_COMPANY_SUCCESS,
            payload: data.companylist,
        });
    }catch (error) {
        dispatch({
            type: ADMIN_COMPANY_FAIL,
            payload:error.response.data.message,
        });
    }
};

//delete company
export const deleteCompany = (id) => async (dispatch) => {
    try{
        dispatch({
            type: DELETE_COMPANY_REQUEST
        });
        const {data} = await axios.delete(`/api/v1/admin/companylist/${id}`);
        dispatch({
            type:DELETE_COMPANY_SUCCESS,
            payload: data.success,
        })
    }catch (error) {
        dispatch({
            type: DELETE_COMPANY_FAIL,
            payload:error.response.data.message,
        })
    }
}


export const getCompanyDetails = (id) => async (dispatch) => {
    try{
        dispatch({
            type: ALL_COMPANY_DETAILS_REQUEST
        });
        const {data} = await axios.get(`http://localhost:5000/api/v1/companylist/${id}`);
        console.log('data');
        console.log(data);
        dispatch({
            type:ALL_COMPANY_DETAILS_SUCCESS,
            payload: data.companylist,
        })
    }catch (error) {
        dispatch({
            type: ALL_COMPANY_DETAILS_FAIL,
            payload:error.response.data.message,
        })
    }
}

//new review
export const newReview = (reviewData) => async (dispatch) => {
    try{
        dispatch({
            type: NEW_REVIEW_REQUEST
        });

        const config = {
            headers: {
                Accept: "application/json",
                "content-type": "application/json",
                "Access-Control-Allow-Credentials": "true",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,OPTIONS,PATCH,DELETE,POST,PUT",
        "Access-Control-Allow-Headers":
            "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version",
        Authorization: `Bearer ${localStorage.getItem("user_token")}`,

                
            },
        
          };

        const {data} = await axios.post(`http://localhost:5000/api/v1/review`, reviewData, config);

        dispatch({
            type: NEW_REVIEW_SUCCESS,
            payload: data.success,
        });
    }catch (error) {
        dispatch({
            type: NEW_REVIEW_FAIL,
            payload:"error.response.data.message",
        })
    }
}

//clearing Errors
export const clearErrors = () => async (dispatch) => {
    dispatch({ type:CLEAR_ERRORS});
};
