import React from "react";
import AppHeader from "../pages/common/header";
import AppFooter from "../pages/common/footer";
import CompanyListing from "../pages/home/companyListing";


function CompanyListings() {
    return (
        <div className="main">
            <AppHeader/>
            <CompanyListing />
            <AppFooter/> 
        </div>
    )
}

export default CompanyListings;