import React, { Fragment, useEffect, useState } from 'react';
import { Row, Col, Rate } from 'antd';
import { useParams } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux";
import { getCompanyDetails, clearErrors,newReview } from '../../actions/companyAction';
import "./id.css"
import AppHeader from '../../pages/common/header';
import AppFooter from '../../pages/common/footer';
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Button,
  } from "@material-ui/core";
import { Rating } from "@material-ui/lab";
import ReviewCard from "./ReviewCard.js";
import { NEW_REVIEW_RESET } from '../../constants/companyConstants';



const CompanyDetails = ({ match }) => {
    const { id } = useParams()

    const dispatch = useDispatch();

    const { companylist, loading, error } = useSelector(
        (state) => state.companyDetails
    );

    const { success, error: reviewError } = useSelector(
        (state) => state.newReview
      );

    const [open, setOpen] = useState(false);
    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState("");

    const submitReviewToggle = () => {
        open ? setOpen(false) : setOpen(true);
      };
    
    const reviewSubmitHandler = () => {
        const myForm = new FormData();
    
        myForm.set("rating", rating);
        myForm.set("comment", comment);
        myForm.set("companylistId", id);
    
        dispatch(newReview(myForm));
    
        setOpen(false);
      };

    useEffect(() => {
        if (error) {
            // alert.error(error);
            dispatch(clearErrors());
          }
      
          if (reviewError) {
            // alert.error(reviewError);
            dispatch(clearErrors());
          }
      
          if (success) {
            alert.success("Review Submitted Successfully");
            dispatch({ type: NEW_REVIEW_RESET });
          }
        dispatch(getCompanyDetails(id));
    }, [dispatch, id]);

    // const options = {
    //     edit: false,
    //     color: "red",
    //     activeColor: "tomato",
    //     size: window.innerWidth < 600 ? 20 : 25,
    //     value: 2.5,
    //     isHalf: true,
    // };

    const options = {
        size: "large",
        value: companylist.ratings,
        readOnly: true,
        precision: 0.5,
      };
console.log(localStorage.getItem("user_token"));
    return (
        <Fragment>
            <AppHeader />
            <Row className='companyDetail'>
                <Col span={18} push={6}>
                    <p> {companylist.introduction}</p>

                    <p> {companylist.email}</p>
                    <p> {companylist.websitelink}</p>
                    <p> {companylist.ratings}</p>
                </Col>
                <Col span={6} pull={18}>
                    {typeof companylist.logo !== 'undefined' ? (
                        <img src={companylist.logo[0].url} alt={companylist.name} />
                    ): null}
                    
                    <p> {companylist.name}</p>
                    <h4>{companylist.category} </h4>
                </Col>
            </Row>
            <Row className='companyDetail2'>
                {typeof companylist.mapEmbededCode !== "undefined" ?
                    <iframe src={companylist.mapEmbededCode} width="640" height="480" /> : "No Map Found"
                }

                <Rate allowHalf value={companylist.ratings}/>
            </Row>
            <h2> Review</h2>

            <button onClick={submitReviewToggle} className="submitReview">
                Submit Review
              </button>

            <Dialog
                aria-labelledby="simple-dialog-title"
                open={open}
                onClose={submitReviewToggle}
            >
                <DialogTitle>Submit Review</DialogTitle>
                <DialogContent className="submitDialog">
                <Rating
                    onChange={(e) => setRating(e.target.value)}
                    name="unique-rating"
                    value={rating}
                    size="large"
                />

                <textarea
                    className="submitDialogTextArea"
                    cols="30"
                    rows="5"
                    value={comment}
                    onChange={(e) => setComment(e.target.value)}
                ></textarea>
                </DialogContent>
                <DialogActions>
                <Button onClick={submitReviewToggle} color="secondary">
                    Cancel
                </Button>
                <Button onClick={reviewSubmitHandler} color="primary">
                    Submit
                </Button>
                </DialogActions>
            </Dialog>
            {companylist.reviews && companylist.reviews[0] ? (
                <div className="reviews">
                {companylist.reviews &&
                    companylist.reviews.map((review) => (
                    <ReviewCard key={review._id} review={review} />
                    ))}
                </div>
            ) : (
                <p className="noReviews">No Reviews Yet</p>
            )}
            <AppFooter />
        </Fragment>
    )
}

export default CompanyDetails;
