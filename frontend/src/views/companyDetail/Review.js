import React, { Fragment, useEffect, useState } from 'react';
import { Row, Col, Rate } from 'antd';
import { useParams } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux";
import { getCompanyDetails, newReview,clearErrors } from '../../actions/companyAction';
import "./id.css"
import AppHeader from '../../pages/common/header';
import AppFooter from '../../pages/common/footer';
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Button,
  } from "@material-ui/core";
import { Rating } from "@material-ui/lab";
import { NEW_REVIEW_RESET } from '../../constants/companyConstants';

const Review = ({ match }) => {
    const { id } = useParams()

    const dispatch = useDispatch();

    const { product, loading, error } = useSelector(
        (state) => state.companyDetails
      );

    const { success, error: reviewError } = useSelector(
        (state) => state.newReview
      );

    useEffect(() => {
        if (error) {
        //   alert.error(error);
          dispatch(clearErrors());
        }
    
        if (reviewError) {
        //   alert.error(reviewError);
          dispatch(clearErrors());
        }
    
        if (success) {
        //   alert.success("Review Submitted Successfully");
          dispatch({ type: NEW_REVIEW_RESET });
        }
        dispatch(getCompanyDetails(match));
      }, [dispatch, match, error, alert, reviewError, success]);


    const [open, setOpen] = useState(false);
    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState("");

    const submitReviewToggle = () => {
        open ? setOpen(false) : setOpen(true);
      };
    
    
    const reviewSubmitHandler = () => {
        const myForm = new FormData();
    
        myForm.set("rating", rating);
        myForm.set("comment", comment);
        myForm.set("productId", match);
    
        dispatch(newReview(myForm));
    
        setOpen(false);
      };
    return (
        <Fragment>
            <button onClick={submitReviewToggle} className="submitReview">
                Submit Review
              </button>
            <h2> Review</h2>
            <Dialog
                aria-labelledby="simple-dialog-title"
                open={open}
                onClose={submitReviewToggle}
            >
                <DialogTitle>Submit Review</DialogTitle>
                <DialogContent className="submitDialog">
                <Rating
                    onChange={(e) => setRating(e.target.value)}
                    value={rating}
                    size="large"
                />

                <textarea
                    className="submitDialogTextArea"
                    cols="30"
                    rows="5"
                    value={comment}
                    onChange={(e) => setComment(e.target.value)}
                ></textarea>
                </DialogContent>
                <DialogActions>
                <Button onClick={submitReviewToggle} color="secondary">
                    Cancel
                </Button>
                <Button onClick={reviewSubmitHandler} color="primary">
                    Submit
                </Button>
                </DialogActions>
            </Dialog>
            <AppFooter />
        </Fragment>
    )
}

export default Review;
