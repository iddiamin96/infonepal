import React from "react";
import AppHeader from "../pages/common/header";
import AppFooter from "../pages/common/footer";
import AddCompany from "../pages/home/addCompanyList";


function ListYourCompany() {
    return (
        <div className="main">
            <AppHeader/>
            <AddCompany />
            <AppFooter/> 
        </div>
    )
}

export default ListYourCompany;