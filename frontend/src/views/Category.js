import React from "react";
import AppHeader from "../pages/common/header";
import AppFooter from "../pages/common/footer";
import BusinessCategory from "../pages/home/businessCategory";


function Categorys() {
    return (
        <div className="main">
            <AppHeader/>
            <BusinessCategory />
            <AppFooter/> 
        </div>
    )
}

export default Categorys;