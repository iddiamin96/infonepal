import React from "react";
import AppHeader from "../pages/common/header";
import AppFooter from "../pages/common/footer";
import "./aboutus.css"


function AboutUs() {
    return (
        <div className="main">
            <AppHeader/>
            <div class="about-section">
                <h1>About Us Page</h1>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book..</p>
            </div>
            <AppFooter/> 
        </div>
    )
}

export default AboutUs;