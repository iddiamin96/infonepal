import React, { useEffect } from "react";
import AppHero from "../pages/home/heroImage";
import CompanyListing from "../pages/home/companyListing";
import BusinessCategory from "../pages/home/businessCategory";
import AddCompany from "../pages/home/addCompanyList";
import AppHeader from "../pages/common/header";
import AppFooter from "../pages/common/footer";
import MetaData from "../pages/common/metaData";
import { getCompany } from "../actions/companyAction";
import { useSelector,useDispatch } from "react-redux";
import HeroSearchSection from "../pages/home/HeroSearchSection";
import {Row, Col} from 'antd';
import useAlert from "react-alert";
import UserOptions from "../pages/common/UserOptions";

function AppHome() {
      const { isAuthenticated, user } = useSelector((state) => state.user);

    const dispatch = useDispatch();
    const value = useSelector(state=>state.companylists);

    useEffect(() => {
        dispatch(getCompany());
    }, [dispatch]);
    return (
        <>
        <div className="main">
            <MetaData title="Info Nepal"/>
            <AppHeader/>
            {isAuthenticated && <UserOptions user={user} />}
            <HeroSearchSection />
            {/* {"console.log(companylists)"}
            {console.log(companylists)} */}

            <div className="titleHolder">
                    <h2> Company Listing </h2>
                    <p> Recent List of company </p>
                </div>
            <div className="listing" gutter={[32, 32]}>
            <CompanyListing companylist={value.companylist} />
            </div>
            <BusinessCategory/>
            <AddCompany/>  
            <AppFooter/> 
        </div>

    </>
        
    )
}

export default AppHome;