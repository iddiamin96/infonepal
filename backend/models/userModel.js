const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true, 'Must provide company name'],
    },
    email:{
        type:String,
        required:[true, 'must provide Email;'],
        unique:true,
        validate:[validator.isEmail, 'must provide Emai;'],
    },
    password:{
        type:String,
        select:false,
        required:[true, 'Must provide your password'],
        minlength: [8, "password must be greater than 8 characters"]
    },
    avatar:[{
        public_id:{
            type:String,
            required:true
        },
        url:{
            type:String,
            required:true
        }
    }],
    role:{
        type:String,
        // required:[true, 'Must select role'],
    },

    createdAt:{
        type:Date,
        default:Date.now,
    },

    resetPasswordToken: String,
    resetPasswordExpired: Date,
})

userSchema.pre("save",async function(next){

    if (!this.isModified("password")){
        next();
    }

    this.password = await bcrypt.hash(this.password,10)
});

//JWT TOKEN
userSchema.methods.getJWTToken = function(){
    return jwt.sign({id:this._id},process.env.JWT_SECRET,{
        expiresIn:process.env.JWT_EXPIRE,
    });
};

//Compare password
userSchema.methods.comparePassword = async function(enterPassword){
    return await bcrypt.compare(enterPassword,this.password);
}

//reset password
userSchema.methods.getResetPasswordToken = function() {
    //generate token
    const resetToken = crypto.randomBytes(20).toString("hex");

    //hashing and adding reset password token to userSchema
    this.resetPasswordToken = crypto.createHash("sha256").update(resetToken).digest("hex");
    this.resetPasswordExpired = Date.now() + 15*60*1000;
    return resetToken;
};

module.exports = mongoose.model("User", userSchema)