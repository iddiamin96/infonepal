const mongoose = require('mongoose');

const ListCompanySchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true, 'Must provide company name'],
    },
    logo:[{
        public_id:{
            type:String,
            required:true
        },
        url:{
            type:String,
            required:true
        }
    }],
    category:{
        type: String,
        required:[true, 'Must select one catrgory'],
    },
    country:{
        type:String,
        required:[true, 'Must provide country name'],
    },
    state:{
        type:String,
        required:[true, 'must provide state name'],
    },
    phoneNumber:{
        type:Number,
        required:[true, 'must provide Phone Number'],
    },
    mobileNumber:{
        type:Number,
        required:[true, 'must provide Mobile Number'],
    },
    email:{
        type:String,
        // required:[true, 'must provide Emai;'],
    },
    websitelink:{
        type:String,
        required:[true, "must provide web address"],
    },
    mapEmbededCode:{
        type:String,
        // required:[true, "must provide EmbededCode of your Location"],
    },
    introduction:{
        type:String,
        // required:[true, "must provide About Company"],
    },
    ratings:{
        type:Number,
        default:0
    },
    stock:{
        type:Number,
        // required:[true,"please enter"]
    },
    numberofReviews:{
        type:Number,
        default:0
    },
    reviews:[
        {
            user:{
                type:mongoose.Schema.ObjectId,
                ref:"User",
                required:true,
            },
            name:{
                type:String,
                // required:true
            },
            rating:{
                type:String,
                default:"0"
            },
            comment:{
                type:String,
                // required:true
            }
        }
    ],
    user:{
        type:mongoose.Schema.ObjectId,
        ref:"User",
        required:true,
    },
    createdAt:{
        type:Date,
        default:Date.now
    },
    pageYOffset:{
        type:String,
    }
})

module.exports = mongoose.model("Companylist", ListCompanySchema)