//create token and save in cookies

const sendJWT = (user, statusCode, res) => {
    const JWTtoken = user.getJWTToken();

    //cookies option
    const options = {
        expires:new Date(
            Date.now() + process.env.COOKIE_EXPIRE * 24 * 60 * 60 * 1000
        ),
        httpOnly:true,
    };

    res.status(statusCode).cookie('token', JWTtoken, options).json({
        success:true,
        user,
        JWTtoken,
    });
}

module.exports = sendJWT;