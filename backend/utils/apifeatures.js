class ApiFeatures{
    constructor(query,querystr){
        this.query = query;
        this.querystr = querystr;
    }

    search(){
        const keyword = this.querystr.keyword ?{
            name:{
                $regex: this.querystr.keyword,
                $options: "i",
            },
        }:{};
        this.query = this.query.find({...keyword});
        return this;
    };

    filter(){
        const queryCopy = {...this.querystr}

        //remove fields from category
        const removeFields = ["keyword", "page", "limit"];

        removeFields.forEach((key)=>delete queryCopy[key]);

        //filter for rating

        this.query = this.query.find(queryCopy);
        return this;
    };

    //show compoany result per page
    pagination(resultPerPage) {
        const currentPage = Number(this.querystr.page) || 1;

        const skip = resultPerPage * (currentPage - 1);

        this.query = this.query.limit(resultPerPage).skip(skip);

        return this;
    }
}



module.exports = ApiFeatures;