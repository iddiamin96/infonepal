const cookieParser = require("cookie-parser");
const cors = require('cors');
const express = require("express");
const app = express();
const errorMiddleware = require("./middleware/error");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");

app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());
// enable cors
app.use(cors());
app.options('*', cors());


//Route imports
const listcompany = require("./routes/listCompanyRoute");
app.use("/api/v1", listcompany);

//user router
const user = require("./routes/userRoute")
app.use("/api/v1",user);

//middleware for error
app.use(errorMiddleware);

module.exports = app;