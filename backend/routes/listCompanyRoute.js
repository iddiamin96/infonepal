const express = require("express");
const { getAllListCompany,createCompanyList,updateCompanyList,deleteCompanyList,getsingleListCompany,createCompanyReview, getCompanyReviews, deleteCompanyReviews, getAdminListCompany } = require("../controllers/listCompanyController");
const { isAuthenticatedUser,roleAuthorization } = require("../middleware/auth");
const router = express.Router();

router.route("/admin/companylists").get( isAuthenticatedUser, roleAuthorization("admin"),getAdminListCompany)

router.route("/admin/companylist").get( getAllListCompany).post( isAuthenticatedUser, roleAuthorization("admin"),createCompanyList);

router.route("/admin/companylist/:id")
.put( isAuthenticatedUser, roleAuthorization("admin"), updateCompanyList)
.delete( isAuthenticatedUser, roleAuthorization("admin"), deleteCompanyList)
router.route("/companylist/:id").get(getsingleListCompany);

router.route("/review").put(isAuthenticatedUser, createCompanyReview).post( createCompanyReview);
router.route("/reviews").get(getCompanyReviews).delete(isAuthenticatedUser, deleteCompanyReviews);

module.exports = router