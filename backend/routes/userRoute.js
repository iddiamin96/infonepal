const express = require("express");
const router = express.Router();
const { registerUser, loginUser, logout, forgotPassword,resetPassword, getUser, updateUserPassword, updateUserProfile, getAllUser, updateUserRole, deleteUser } = require("../controllers/userController");
const { isAuthenticatedUser, roleAuthorization } = require("../middleware/auth");

router.route("/register").post(registerUser);

router.route("/login").post(loginUser);

router.route("/logout").get(logout);

router.route("/password/forgot").post(forgotPassword);
router.route("/password/reset/:token").put(resetPassword);
router.route("/profile").get(isAuthenticatedUser,getUser)
router.route("/password/update").put(isAuthenticatedUser, updateUserPassword);
router.route("/profile/update").put(isAuthenticatedUser, updateUserProfile);
router.route("/admin/users").get(isAuthenticatedUser, roleAuthorization("admin"),getAllUser);
router.route("/admin/user/:id").get(isAuthenticatedUser, roleAuthorization("admin"),getUser)
        .put(isAuthenticatedUser, roleAuthorization("admin"),updateUserRole)
        .delete(isAuthenticatedUser, roleAuthorization("admin"),deleteUser);


module.exports = router;
