const User = require("../models/userModel");
const ErrorHandler = require("../utils/errorhandler");
const catchAsyncErrors = require("../middleware/catchAsyncErrors");
const sendToken = require("../utils/JWT");
const sendEmail = require("../utils/sendEmail.js");
const crypto = require("crypto");
const cloudinary = require("cloudinary");



//Register as User
exports.registerUser = catchAsyncErrors(async(req, res, next) =>{

  const myCloud = await cloudinary.v2.uploader.upload(req.body.avatar, {
    folder: "avatars",
    width: 150,
    crop: "scale",
  });

     const {name, email, password,role} = req.body;
     const user = await User.create({
         name,
         email,
         password,
         avatar: {
             public_id: myCloud.public_id, 
             url: myCloud.secure_url,
         },
         role,
     });

     sendToken(user,201, res);
});

//User LOgin
exports.loginUser = catchAsyncErrors(async(req, res, next) =>{
    const {email, password} = req.body;

    //blank email and password
    if(!email || !password){
        return next(new ErrorHandler("please Enter correct email and password", 400));
    }

    const user = await User.findOne({ email }).select("+password");

    if(!user){
        return next(new ErrorHandler("invalid user",401));
    }
    const isPasswordMatched = await user.comparePassword(password);

    if(!isPasswordMatched){
        return next(new ErrorHandler("invalid password",401));
    };

    sendToken(user,200, res);
});

//user logout

exports.logout = catchAsyncErrors(async (req, res, next) => {
    res.cookie("token", null, {
      expires: new Date(Date.now()),
      httpOnly: true,
    });
  
    res.status(200).json({
      success: true,
      message: "Logged Out",
    });
  });

  //forget Password
exports.forgotPassword = catchAsyncErrors(async (req,res,next) => {
  const user = await User.findOne({ email: req.body.email});

    if(!user){
        return next(new ErrorHandler("User not found, 404"));
    }

  //get reset password token
  const resetToken = user.getResetPasswordToken();
    await user.save({validateBeforeSave: false});

  const resetPasswordUrl = `${req.protocol}://${req.get("host")}/api/v1/password/reset/${resetToken}`;
  const message = `your password reset token is :- \n\n ${resetPasswordUrl}`;
    try{
        await sendEmail({
            email:user.email,
            subject:`password recovery`,
            message,
        });

        res.status(200).json({
            success : true,
            message: `Email sent to ${user.email}`
        })
    }catch(error){
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;

        await user.save({ validateBeforeSave : false });
        return next(new ErrorHandler(error.message, 500));
    }
})

//reset password
exports.resetPassword = catchAsyncErrors(async (req, res, next) => {
    // creating token hash
    const resetPasswordToken = crypto
      .createHash("sha256")
      .update(req.params.token)
      .digest("hex");
  
    const user = await User.findOne({
      resetPasswordToken,
      resetPasswordExpire: { $gt: Date.now() },
    });
  
    if (!user) {
      return next(
        new ErrorHandler(
          "Reset Password Token is invalid or has been expired",
          400
        )
      );
    }
  
    if (req.body.password !== req.body.confirmPassword) {
      return next(new ErrorHandler("Password does not password", 400));
    }
  
    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
  
    await user.save();
  
    sendToken(user, 200, res);
  });

  //get user detail
exports.getUser = catchAsyncErrors(async (req, res, next) => {
    const user = await User.findById(req.user.id);

    res.status(200).json({ 
        success:true,
        user,
    });
});

 //update user password
exports.updateUserPassword = catchAsyncErrors(async (req, res, next) => {
  const user = await User.findById(req.user.id).select("+password");

  const isPasswordMatched = await user.comparePassword(req.body.oldPassword);

    if(!isPasswordMatched){
        return next(new ErrorHandler("old password is incorrerct",400));
    };

    if(req.body.newPassword !== req.body.confirmPassword) {
      return next(new ErrorHandler("password not match", 400));
    }

    user.password = req.body.newPassword;

    await user.save();

    sendToken(user, 200, res);
});
  
 //update user profile
 exports.updateUserProfile = catchAsyncErrors(async (req, res, next) => {

  const newProfile ={
    name: req.body.name,
    email: req.body.email,
  };

  if (req.body.avatar !== "") {
    const user = await User.findById(req.params.id);

    const imageId = user.avatar.public_id;

    await cloudinary.v2.uploader.destroy(imageId);

    const myCloud = await cloudinary.v2.uploader.upload(req.body.avatar, {
      folder: "avatars",
      width: 150,
      crop: "scale",
    });

    newUserData.avatar = {
      public_id: myCloud.public_id,
      url: myCloud.secure_url,
    };
  }

  const user = await User.findByIdAndUpdate(req.user.id, newProfile,{
    new:true,
    runValidators: true,
    userFindAndModify: false,
  });
  res.status(200).json({ 
    success:true
  });
});

// Admin get all user detail
exports.getAllUser = catchAsyncErrors(async (req, res, next) => {
    const users = await User.find();

    res.status(200).json({ 
        success:true,
        users,
    });
});


  //Admin get user detail
  exports.getUser = catchAsyncErrors(async (req, res, next) => {
    const user = await User.findById(req.user.id);

    if(!user){
      return next(new ErrorHandler(`User not exist with id : ${req.user.id}`));
  }

    res.status(200).json({ 
        success:true,
        user,
    });
});

 //admin update user role
 exports.updateUserRole = catchAsyncErrors(async (req, res, next) => {

  const newProfile ={
    name: req.body.name,
    email: req.body.email,
    role: req.body.role,
  }
  const user = await User.findByIdAndUpdate(req.user.id, newProfile,{
    new:true,
    runValidators: true,
    userFindAndModify: false,
  });
  res.status(200).json({ 
    success:true
  });
});

 //admin delete user
 exports.deleteUser = catchAsyncErrors(async (req, res, next) => {

  const user = await User.findById(req.params.id);

  if(!user){
    return next(new ErrorHandler(`User not exist with id : ${req.user.id}`));
  }
  const imageId = user.avatar.public_id;

  await cloudinary.v2.uploader.destroy(imageId);

  await user.remove();

  res.status(200).json({ 
    success:true,
    message:"user deleated successfully"
  });
});

