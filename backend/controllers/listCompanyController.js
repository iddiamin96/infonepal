const Companylist = require("../models/listCompanyModel");
const ErrorHandler = require("../utils/errorhandler");
const catchAsyncErrors = require("../middleware/catchAsyncErrors");
const ApiFeatures = require("../utils/apifeatures");
const cloudinary = require("cloudinary");

//create company list by admin
exports.createCompanyList = catchAsyncErrors(async (req, res,next) => {

    let logos = [];

    if (typeof req.body.logos === "string") {
        logos.push(req.body.logos);
    } else {
        logos = req.body.logos;
    }

    const logosLinks = [];

    for (let i = 0; i < logos.length; i++) {
        const result = await cloudinary.v2.uploader.upload(logos[i], {
        folder: "companys",
        });

        logosLinks.push({
        public_id: result.public_id,
        url: result.secure_url,
        });
    }

    req.body.logos = logosLinks;
    req.body.user = req.user.id;

    const companylist = await Companylist.create(req.body);

    res.status(201).json({ 
        success:true,
        companylist })
})


//get all company

exports.getAllListCompany = catchAsyncErrors(async (req, res, next) => {
    const resultPerPage = 8;
    const companyListCount = await Companylist.countDocuments(); 
    const apiFeature = new ApiFeatures(Companylist.find(), req.query)
    .search()
    .filter().pagination(resultPerPage);

    const companylist = await apiFeature.query;

        res.status(200).json({ 
            success:true,
            companylist,
            companyListCount, })
});

//get all company (admin)
exports.getAdminListCompany = catchAsyncErrors(async (req, res, next) => {
    const companylists = await Companylist.find();

        res.status(200).json({ 
            success:true,
            companylists,
        })

});

//get single company
exports.getsingleListCompany = catchAsyncErrors(async (req, res, next) => {
    const companylist = await Companylist.findById(req.params.id);
    if(!companylist){
        return next(new ErrorHandler("company not found", 404))
    }
    res.status(200).json({
        success:true,
        companylist,
    });
});

//update company list by admin
exports.updateCompanyList = catchAsyncErrors(async (req, res, next) => {
        let companylist = await Companylist.findById(req.params.id);
        if(!companylist){
            return next(new ErrorHandler("company not found", 404))
        }
    companylist= await Companylist.findByIdAndUpdate(req.params.id,req.body,{
            new:true,
            runValidators:true,
            useFindAndModify:false
        });
        res.status(200).json({
            success:true,
            companylist
        })
})

//delete company
exports.deleteCompanyList = catchAsyncErrors(async (req, res, next) => {
    const companylist = await Companylist.findById(req.params.id);
    if(!companylist){
        return next(new ErrorHandler("company not found", 404))
    }

    // Deleting Images From Cloudinary
    for (let i = 0; i < companylist.logos.length; i++) {
        await cloudinary.v2.uploader.destroy(companylist.logos[i].public_id);
    }

    await companylist.remove();
    res.status(200).json({
        success:true,
        message:"company list deleted"
    })
})

//create review and update review
exports.createCompanyReview = catchAsyncErrors(async (req, res, next) => {

    const { rating, comment, companyId } = req.body;
  
    const review = {
    //   user:req.user._id,
    //   name: req.user.name,
      rating,
      comment,
    };

    const companylist = await Companylist.find({
        _id: companyId
    });
    console.log("companylist")
    console.log(companylist)
    const isReviewed = companylist.reviews.find((review)=> review.user.toString() === req.user._id.toString())
  
    if(isReviewed){
      companylist.reviews.forEach((review)=> {
        if(review.user.toString() === req.user._id.toString())
        (review.rating=rating),
        (review.comment=comment);
      })
  
    }else{
      companylist.reviews.push(review);
      companylist.numberofReviews = companylist.reviews.length;
    }
  
    let average = 0;
    companylist.reviews.forEach((review) => {
      average += review.rating;
    }) 
    companylist.ratings = average / companylist.reviews.length;
  
    await companylist.save({
      validateBeforeSave:false
    })
  
    res.status(200).json({ 
      success:true,
  
    });
  });

  //get all reviewed company

exports.getCompanyReviews = catchAsyncErrors(async (req, res, next) => {
    const companylist = await Companylist.findById(req.query.id);
    if(!companylist){
        return next(new ErrorHandler("company not found", 404));
    }

    res.status(200).json({
        success:true,
        reviews: companylist.reviews,
    });
})

  //delete review
exports.deleteCompanyReviews = catchAsyncErrors(async (req, res, next) => {
    const companylist = await Companylist.findById(req.query.companyId);
    if(!companylist){
        return next(new ErrorHandler("company not found", 404))
    }

    const reviews = companylist.reviews.filter(
        (review)=> review._id.toString() === req.query._id.toString()
    );

    let average = 0;

    reviews.forEach((review) => {
      average += review.rating;
    });

    const ratings = average / reviews.length;

    const numberofReviews = reviews.length;

    await Companylist.findByIdAndUpdate(
        req.query.companyId,
        {
            reviews,
            ratings,
            numberofReviews,
        },
        {
            new:true,
            runValidators:true,
            useFindAndModify:false,
        }
    );

    res.status(200).json({
        success:true,
    })
})
  